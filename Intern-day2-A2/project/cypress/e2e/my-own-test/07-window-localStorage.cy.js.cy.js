///<reference types="cypress" />

const token =
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFyeXN0ayIsIl9pZCI6IjY1YzBjMTYzYTVmYzYyMDAwYzYyODY1NSIsIm5hbWUiOiJZdXN0aWthIEFrYmFyIFJpbmkiLCJpYXQiOjE3MDcxMzEyMzYsImV4cCI6MTcxMjMxNTIzNn0.10PZiWGrgBrcq9lbZyNmtu6_egZJFnL5COmkcwXIoW4'

    describe('Basic Authenticated Desktop Test', () => {
        before(() => {
            cy.then(() => {
                window.localStorage.setItem('__auth__token', token)
        })
    })

    beforeEach(() => {
        // bootstrapping external things
        cy.viewport(1280, 720)

        cy.visit('https://codedamn.com')
    })
    
    it('Should pass', () => {})
})